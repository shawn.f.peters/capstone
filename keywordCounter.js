// Uses Mongo shell, so cannot import data from json files using 'require'

conn = new Mongo();
db = connect("localhost:27017/capstone");

const collections = ["credentials", "exploits", "financial", "internetaccounts", "other", "pii", "videogameaccounts", "all"];
const keywords = {"credentials":
{"path": "data/credentials/",
"keywords": ["id", "pw", "pass", "password", "passwords", "pass word", "forgot", "passcode", "pass code", "login", "authentication", "auth",
    "account", "username", "user", "users", "profile", "2fa", "two factor",  "alarm", "alert", "guid", "uuid"]},
"exploits": 
{"path": "data/exploits/",
"keywords":["database", "filezilla", "prompt", "powershell", "payload", "domain", "exploit", "firewall", "malware", "ransomware", "botnet", 
    "ddos", "encryption", "phishing", "compile", "data", "hack", "hacker", "http", "port", "router", "modem", "admin", "root", "security",
    "guid", "uuid", "auth", "javascript", "java", "python"]},
"financial": 
{"path": "data/financial/",
"keywords": ["btc", "bitcoin", "wallet", "crypto", "credit", "bank", "ethereum", "visa", "mastercard", "coinbase", "litecoin", "ltc", 
    "eth", "xrp", "bitcoin cash", "bch", "bitcoin sv", "bsv", "tether", "usdt", "eos", "binance coin", "bnb", "tezos", "xtz", 
    "cardano", "ada", "cainlink", "link", "stellar", "lumens", "xlm", "monero", "xmr", "poloniex", "kraken", "bitfinex", "cex", "exchange", 
    "cryptocurrency", "transaction", "discover", "american express", "amex", "money", "venmo", "pay pal", "paypal", "gold", "silver", 
    "bronze", "diamond", "diamonds", "withdrawal", "finance", "financial", "loan", "balance", "deposit", "withdraw", "vanguard", "fidelity", 
    "etf", "stock", "mutual", "fund", "atm", "debit", "invest"]},
"internetaccounts": 
{"path": "data/internet_accounts/",
"keywords": ["psn", "switch", "hotmail", "gmail", "outlook", "yahoo", "google", "iphone", "icloud", "amazon", "aws","linkedin", "linked in", 
    "facebook", "twitter", "instagram", "insta", "profile", "vanguard", "fidelity"]},
"other": 
{"path": "data/other/",
"keywords": ["department", "president"]},
"pii": 
{"path": "data/pii/",
"keywords": ["address", "visa", "ssn", "social security", "id", "pw", "pass", "password", "pass word", "acct", "full name", "code", 
    "account", "phone", "state", "country", "city", "identification", "passport", "citizen", "firstname", "last name", "lastname", 
    "email", "email address", "profile", "zip", "zip code", "first name", "postal code"]},
"videogameaccounts": 
{"path": "data/video_game_accounts/",
"keywords": ["epic", "fortnite", "final fantasy", "xiv", "ffxiv", "csgo", "warcraft", "runescape", "pubg", "eve online", "psn", "switch", 
    "destiny", "warframe", "playstation", "xbox", "gold", "silver", "bronze", "diamond", "diamonds", "code", "sony", "microsoft", "nintendo", 
    "ubisoft"]},
"all":
{"path": "data/all/",
"keywords": ["id", "pw", "pass", "password", "passwords", "pass word", "forgot", "passcode", "pass code", "login", "authentication", "auth", 
    "account", "username", "user", "users", "2fa", "two factor",  "alarm", "alert", "guid", "uuid", "epic", "fortnite", "final fantasy", "xiv", 
    "database", "filezilla", "prompt", "powershell", "payload", "domain", "exploit", "firewall", "malware", "ransomware", "botnet", 
    "ddos", "encryption", "phishing", "compile", "data", "hack", "hacker", "http", "port", "router", "modem", "admin", "root", "security",
    "guid", "uuid", "javascript", "java", "python", "ffxiv", "csgo", "warcraft", "runescape", "pubg", "eve online", "psn", "switch", 
    "btc", "bitcoin", "wallet", "crypto", "credit", "bank", "ethereum", "visa", "mastercard", "coinbase", "litecoin", "ltc", "eth", "xrp", 
    "bitcoin cash", "bch", "bitcoin sv", "bsv", "tether", "usdt", "eos", "binance coin", "bnb", "tezos", "xtz", "cardano", "ada", "cainlink", 
    "link", "stellar", "lumens", "xlm", "monero", "xmr", "poloniex", "kraken", "bitfinex", "cex", "exchange", "cryptocurrency", "transaction", 
    "discover", "american express", "amex", "money", "venmo", "pay pal", "paypal", "withdrawal", "finance", "financial", "loan", "balance", 
    "deposit", "withdraw", "vanguard", "fidelity", "etf", "stock", "department", "president", "mutual", "fund", "atm", "debit", "invest", 
    "destiny", "warframe", "playstation", "xbox", "gold", "silver", "bronze", "diamond", "diamonds", "code", "sony", "microsoft", "nintendo", 
    "ubisoft", "address", "ssn", "social security", "acct", "full name", "phone", "state", "country", "city", "identification", "passport", 
    "citizen", "firstname", "last name", "lastname", "email", "email address", "zip", "zip code", "first name", "postal code", "psn", "switch", "hotmail", "gmail", "outlook", "yahoo", "google", "iphone", "icloud", "amazon", "aws","linkedin", "linked in", 
    "facebook", "twitter", "instagram", "insta", "profile"]}
}


printjson("{'collections':");
collections.forEach(collection => {
    const coll = (`'${collection}':{`);
    printjson(`${coll}`);
    keywords[collection].keywords.forEach(word => {
        const count = db[collection].find({"data.matches": word}).count();
        let jsonVal = {};
        if (word == keywords[collection].keywords.slice(-1)[0]) {
            jsonVal = `'${word}': ${count}`;
        } else {
            jsonVal = `'${word}': ${count},`;
        }
        printjson(jsonVal);
    });
    if (collection == collections.slice(-1)[0]) {
        printjson("}");
    } else {
        printjson("},");
    }
});

printjson("}");