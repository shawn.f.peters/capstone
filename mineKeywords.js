const { exec } = require('child_process');
const fs = require('fs');
const ipRegex = require('ip-regex');
const _ = require('lodash');

const mineKeywords = (infile, keywords, outpath) => {
    // Make sure directories exist
    const dirStruct = outpath.substring(0, outpath.lastIndexOf("/"));
    if (!fs.existsSync(dirStruct)){
        fs.mkdir(dirStruct, { recursive: true }, (err) => {
            if (err) throw err;
        });
    }

    // Analyze against keywords
    let i = 0;
    infile.forEach(line => {
        const intersect = _.intersection(_.words(_.toLower(line.text)), keywords);
        if(intersect.length > 0) {
            i++;
            const jsonLine = {"id": i, "data": {"matches": intersect, "url": line.url, "text": line.text}};
            jsonWriter(outpath, JSON.stringify(jsonLine));
        }
    });
}
  
function jsonWriter(file, line) {
    fs.appendFile(file, "\n" + line, (err) => {
        if (err) {
            return console.log(err);
        }
    })
}

module.exports = { mineKeywords };

/*    
    // Check if there are email addresses in scraped text
    function getEmails(infile, emailOutfile, imageSrc) {
        let emails = string.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
        if (emails != null && emails.length) {
            emails.forEach(emailAddr => {
                const emailJson = JSON.stringify({url: imageSrc, email: emailAddr});
                jsonWriter(emailOutfile, emailJson);
            })
        } else {
            return false;
        }
    }
    // Check if there are IP addresses in the scraped text
    function getIPAddress(imageString) {
        let ipAddrs;
        if(ipRegex().test(imageString)) {
            ipAddrs = imageString.match(ipRegex());
            return ipAddrs;
        } else {
            return false;
        }
    }
    
    // Run a whois on any IP addresses we find and write to a file
    function runWhois(ips, ipOutfile, imageSrc) {
        ips.forEach(ip => {
            // run whois
            exec('whois ' + ip, (err, stdout) => {
                if (err) {
                  console.error(err);
                } else {
                 const standardOut = stdout;
                 const whoJson = JSON.stringify({url: imageSrc, ip: ip, whois: standardOut});
                 jsonWriter(ipOutfile, whoJson);
              }
            });
        });
    }
}
    */
