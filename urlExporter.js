// Uses Mongo shell, so cannot import data from json files using 'require'

conn = new Mongo();
db = connect("localhost:27017/capstone");

const keyword = "password"

q = db.all.find({"data.matches":keyword}, {"data.url":1})

while(q.hasNext()) {
    printjson(q.next().data.url);
}