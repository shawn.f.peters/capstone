# Import data and add column names
all_counts <- read.csv('../keywords/counts.csv', header = FALSE)
colnames(all_counts) <- c("collection", "keyword", "count")

# Sort by count descending
sorted_counts <- all_counts[order(all_counts$count, decreasing = TRUE),]
sorted_counts[1:10,]

sorted_counts[which(sorted_counts$keyword == "ssn"),]

