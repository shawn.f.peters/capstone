const fs = require('fs');
const { mineKeywords } = require('./mineKeywords.js');
const readlineSync = require('readline-sync');

const KEYWORDS = require('./keywords/keywords.json');
const SCRAPED_DATA_PATH = require('./config/paths.json');

/* Get keyword list and input & output files */
const USER_IN = readlineSync.question("Enter keyword type, input json file name, output json file name as arguments: ");
const ARGS = USER_IN.split(" ");
const IN_NAME = ARGS[1]
const OUT_NAME = ARGS[2];

/* Validate user input */
if (!OUT_NAME.match(/\.json$/i) || !IN_NAME.match(/\.json$/i)) {
    console.error("Invalid file name.  Must be .json.");
    process.exit(1);

}

/* Switch to pass the correct keywords and outpath to miner */
switch (ARGS[0]) {
    case "credentials":
        main(KEYWORDS.credentials.keywords, KEYWORDS.credentials.path);
        break;
    case "exploits":
        main(KEYWORDS.exploits.keywords, KEYWORDS.exploits.path);
        break;
    case "financial":
        main(KEYWORDS.financial.keywords, KEYWORDS.financial.path);
        break;
    case "internetaccounts":
        main(KEYWORDS.internetAccounts.keywords, KEYWORDS.internetAccounts.path);
        break;
    case "other":
        main(KEYWORDS.other.keywords, KEYWORDS.other.path);
        break;
    case "pii":
        main(KEYWORDS.pii.keywords, KEYWORDS.pii.path);
        break;
    case "vga":
        main(KEYWORDS.videoGameAccounts.keywords, KEYWORDS.videoGameAccounts.path);
        break;
    case "all":
        main(KEYWORDS.all.keywords, KEYWORDS.all.path);
        break;
    default:
        console.error("Invalid argument, use one of the following: \n credentials \n exploits \n financial \n internetaccounts \n other \n pii \n vga \n all");
        break;
}


function main(kws, savePath) {

    const INFILE = require(SCRAPED_DATA_PATH.inputPath + IN_NAME);
    const OUTFILE = savePath + OUT_NAME;

    console.log("Refining '%s'...", SCRAPED_DATA_PATH.inputPath + IN_NAME);
    
    mineKeywords(INFILE, kws, OUTFILE);

    console.log("Refined '%s'.", SCRAPED_DATA_PATH.inputPath + IN_NAME);
    console.log("Refined files written to '%s'.", OUTFILE);
}
