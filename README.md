# capstone

prnt.sc image hosting site text scraper and data aggregator.  The Node portion of the project takes JSON files of URLs and OCR text data and searches for specific keywords.  If there are matches for a given keyword collection, an element is added to an output JSON in an appropriately labeled directory.  These JSON files are then loaded into MongoDB for further analysis.


## TODO
1. Import all data into mongo
2. Build front end for search functionality with regex
3. Return URLs & load paginated image results

## Initial Data Analysis

Count total number of data points that we have across all files using native Unix `wc` command.

`wc -l <filename>`

File | Size (MB) | Count
---- | ---- | ----
03252020.json | 46.2 | 97356
03282020.json | 41.5 | 87997
03312020.json | 46.7 | 98593
04042020.json | 45.3 | 94889
04082020.json | 49.9 | 104892
04122020.json | 47.1 | 99048
04152020.json | 46.6 | 98309
04182020.json | 51.9 | 108605
04222020.json | 46.3 | 97301
04262020.json | 50.1 | 104752
04302020.json | 54.1 | 113625
05032020.json | 40.2 | 83952
Total | 555.9 | 1189319

Total count of datapoints with at least one keyword match.

File | Size (MB) | Count
---- | ---- | ----
all-03252020.json | 19.5 | 19081
all-03282020.json | 17.4 | 17225
all-03312020.json | 19.7 | 19507
all-04042020.json | 19.4 | 18868
all-04082020.json | 21.1 | 20664
all-04122020.json | 20.2 | 19806
all-04152020.json | 19.8 | 19541
all-04182020.json | 22.2 | 21774
all-04222020.json | 19.7 | 19457
all-04262020.json | 21.5 | 21107
all-04302020.json | 23.1 | 22853
all-05032020.json | 17.2 | 16843
Total | 240.8 | 236726

## Execution

From the root of the project execute:

`npm init`

Once all of the packages and dependencies are installed, the script can be executed from the root of the project with:

`node index.js`

When prompted enter a keyword category, the name of the input file, and the name for the output file:

`all 03252020.json all03252020.json`

**Note:** If the size of the output file exceeds about 60MB the script will fail due to a file table overflow.

## Output
An element of the output JSON is of the form:

`{"id": i, "data": {"matches": intersect, "url": line.url, "text": line.text}`


## Categories

Categories of keywords to match against and their corresponding command line argument:

Data | Argument
---- | ---- 
Credentials | `credentials`
Exploits | `exploits`
Financial | `financial`
Internet Accounts | `internetaccounts`
Other | `other`
Personally Identifiable Information (PII) | `pii`
Video Game Accounts | `vga`
All | `all`


# Database

MongoDB is used for data storage and some analysis after cleaning the complete data set.

### Setup

1. Install Mongo on macOS

`brew tap mongodb/brew`

`brew install mongodb-community@4.2`

2. Start Mongo

`brew services start mongodb-community@4.2`

3. Create a database and add the collections of data 

`mongoimport --db <database> --collection <collection> --file /path/to/data.txt`


### Querying the Database
By keyword:

`db.<collection>.find({"data.matches": "<keyword>"})`

Count matches for keyword in a given collection:

`db.<collection>.find({"data.matches":"<keyword>"}).count()`

Return only "matches" and "url" for a query:

`db.all.find({"data.matches":"ssn"}, {"data.matches":1, "data.url":1})`

Regex for any term and return url:

`db.blob.find({"text": { $regex: "<term>"}}, {"url": 1})`

## Keyword Counter
Gets frequency of keywords in database and writes to a JSON file.  Since this is a Mongo script, we cannot import the JSON file of keywords as a variable using `require` so we need to house the keywords object in the script itself.

To write the results to a file, navigate to the project root on the command line and execute:

`mongo --quiet keywordCounter.js > raw_count.txt`

The resulting `raw_count.txt` file needs to be cleaned up.  Run `cleanMongoJson` and copy the results from the command line into a json file.

`npm run cleanMongoJson './keywords/raw_count.txt'`

## Data to CSV
The cleaned JSON can then be further filtered.  Use `writeToCSV` and pass a lower threshold value for the number of matches.  This way we can see the most common results and do some analysis and plotting in R.

`npm run writeToCSV '/full/path/to/keywords/clean_counts.json' '/full/path/to/keywords/counts.csv' 1000`

The above example will write the results with over 1000 matches to the `counts.csv` file in the format:

`collection,keyword,count`

Ex:

`pii,email,13422`

## imageLooper
Point to txt file of cleaned URLs to loop through the images in a browser. `inFilePath` must be set manually in the script. A starting index can be set as well (default 0).

`node tools/imageLooper.js`

## imageCurler
Reads indices of selected URLs from a `results/urls/results.json` and uses cURL to download the images for a given keyword. Images are saved to the `/tmp/` directory. `KEYWORD` must be set manually in the script.

`node tools/imageCurler.js`

## Notes
2020-05-09 Had to reformat the INPUT json files to load into mongo because they were arrays.  Stripped off the square brackets and removed the commas.