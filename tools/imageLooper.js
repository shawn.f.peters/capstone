const puppeteer = require('puppeteer');
const fs = require('fs');
const PATHS = require('../config/paths.json');

const INFILEPATH = PATHS.workingDir + "results/urls/52-root.txt";
const startIndex = 0;


function getMaxVariance(max) {
    max = Math.floor(max);
    return Math.floor(Math.random() * (max + 1));
  }

function delay(time, variance) {
    let additionalDelay = getMaxVariance(variance);
    return new Promise(function(resolve) { 
        setTimeout(resolve, time + additionalDelay);
    });
 }

(async () => {
    const data = fs.readFileSync(INFILEPATH, 'utf-8');
    
    const urls = data.split("\n");

    const browser = await puppeteer.launch({headless: false});

    const page = await browser.newPage();
    await page.setDefaultNavigationTimeout(0);
    page.setViewport({ width: 1080, height: 960 });

    for (let index = startIndex; index < urls.length; index++) {
        console.log(index + " / " + urls.length + " - " + urls[index]);
        await page.goto(urls[index]);
        await delay(5000,0);
    }
})();