const fs = require('fs');

const cleanMongoJson = (inFilePath) => {
    const data = fs.readFileSync(inFilePath, 'utf8');
    const lines = data.split("\n");
    lines.forEach(line => {
        //let strip = line.substring(1, line.length - 1);
        //strip = strip.replace(/'/g, "\"");
        let strip = line.substring(0, line.length - 1);
        console.log(strip);
    });
}

module.exports = { cleanMongoJson }