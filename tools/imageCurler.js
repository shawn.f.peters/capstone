const { exec } = require("child_process");
const fs = require('fs');
const PATHS = require('../config/paths.json');

const INDICES = require('../results/urls/results.json');
const INFILEPATH = PATHS.workingDir + "results/urls/52-root.txt";
const KEYWORD = INFILEPATH.split("-")[1].split(".")[0];

const data = fs.readFileSync(INFILEPATH, 'utf-8');    
const urls = data.split("\n");

INDICES[KEYWORD].forEach(index => {
    //console.log(urls[index]);
    const i = index;
    const name = urls[i].split("/").slice(-1)[0];
    
    exec(`curl -k ${urls[i]} -X GET -o /tmp/${name}`, (error, stdout, stderr) => {
        if (error) {
            console.log(`error: ${error.message}`);
            return;
        }
        if (stderr) {
            console.log(`stderr: ${stderr}`);
            return;
        }
        console.log(`stdout: ${stdout}`);
    });
    
});
