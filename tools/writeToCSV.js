const fs = require('fs');

const writeToCSV = (infile, outfile, threshold) => {
    const ALL_COUNTS = require(infile);
    const collections = Object.keys(ALL_COUNTS.collections);
    collections.forEach(collection => {
        const keywords = Object.keys(ALL_COUNTS.collections[collection]);
        keywords.forEach(keyword => {
            const count = ALL_COUNTS.collections[collection][keyword];
            if (count >= threshold) {
                const line = collection + "," + keyword + "," + count + "\n";
                fs.appendFileSync(outfile, line);
            }
        });
    });
}

module.exports = { writeToCSV }